(function($) {
    $.fn.formToWizard = function(options) {
        options = $.extend({  
            submitButton: "",
            pagerClass: "",
            prevText: "&lt Back",
            nextText: "Next &gt",
            stepText: "Step"
        }, options); 
        
        var element = this;

        var steps = $(element).find("fieldset");
        var count = steps.size();

        // Do nothing if we have 0 or 1 fieldset
        if (count < 2) {
            return;
        }

        var submmitButtonName = "#" + options.submitButton;
        $(submmitButtonName).hide();

        // 2
        $element = $(element);        
        $element.wrap('<div class="form-wizard"></div>');
        $element.before("<ul class='steps'></ul>");

        steps.each(function(i) {
            $(this).wrap("<div id='step" + i + "'></div>");
            $(this).append("<p id='step" + i + "commands'></p>");

            // 2
            var name = $(this).find("legend").html();
            $(".steps").append("<li id='stepDesc" + i + "'>"+options.stepText+" " + (i + 1)+"/"+ count + "<span>" + name + "</span></li>");

            if (i == 0) {
                createNextButton(i);
                selectStep(i);
            }
            else if (i == count - 1) {
                $("#step" + i).hide();
                createPrevButton(i);
            }
            else {
                $("#step" + i).hide();
                createPrevButton(i);
                createNextButton(i);
            }
        });

        function createPrevButton(i) {
            var stepName = "step" + i;
            $("#" + stepName + "commands").append("<div id='" + stepName + "Prev' class='btn prev "+options.pagerClass+"'>"+options.prevText+"</div>");

            $("#" + stepName + "Prev").bind("click", function(e) {
                $("#" + stepName).hide();
                $("#step" + (i - 1)).show();
                $(submmitButtonName).hide();
                selectStep(i - 1);
            });
        }

        function createNextButton(i) {
            var stepName = "step" + i;
            $("#" + stepName + "commands").append("<div id='" + stepName + "Next' class='btn next "+options.pagerClass+"'>"+options.nextText+"</div>");

            $("#" + stepName + "Next").bind("click", function(e) {
                $("#" + stepName).hide();
                $("#step" + (i + 1)).show();
                if (i + 2 == count) {
                    $(submmitButtonName).show();
                }
                selectStep(i + 1);
            });
        }

        function selectStep(i) {
            $(".steps li").removeClass("current");
            $("#stepDesc" + i).addClass("current");
        }

    }

  Drupal.behaviors.formToWizard = {
    attach: function (context, settings) {
      Drupal.settings.formsToWizard = Drupal.settings.formsToWizard || {};

      $(Drupal.settings.formsToWizard, context).each(function (i, form) {
          console.log(form);
        $('#' + form[0]).formToWizard({submitButton: 'edit-' + form[1]});
      });

    }
  }

})(jQuery);
