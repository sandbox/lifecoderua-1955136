This module provide a way to divide regular drupal form into several steps.
All you need is to set up several fieldsets,and each of them become a single
step.

Usage:

function mymodule_example_form($form, &$form_state) {
  $form = array(
    // Regular drupal form definition
    'submit' => array(
      '#type' => 'submit',
      '#value' => 'Submit'
    )
  );
  
  return $form;
}

function multistep_form(){
	return drupal_get_wizard_form('mymodule_example_form', 'submit');	
}

?>
